import "./style.css";

import createHeader from "./header/header";
const app = document.getElementById("app");
const header = createHeader();
app.appendChild(header);

import createMain from "./main/main";
const main = createMain();
app.appendChild(main);

import createFooter from "./footer/footer";
const footer = createFooter();
app.appendChild(footer);
