import "./header.css";
import Logo from "../assets/logo.svg";
import createElement from "../utils";
import twitter from "../assets/twitter.svg";
import inst from "../assets/inst.svg";
import facebook from "../assets/facebook.svg";

const createHeader = () => {
  const header = document.createElement("header");
  header.classList.add("header");

  const logoImg = document.createElement("img");
  logoImg.classList.add("header__logo");
  logoImg.src = Logo;
  logoImg.alt = "Logo";

  const headerlinks = document.createElement("ul");
  headerlinks.classList.add("header__links");

  const woocommerceLi = document.createElement("li");
  woocommerceLi.classList.add("header__li");
  const woocommerce = document.createElement("a");
  woocommerce.textContent = "W00C0MMERCE";
  woocommerceLi.appendChild(woocommerce);

  const productLi = document.createElement("li");
  productLi.classList.add("header__li");
  const product = document.createElement("a");
  product.textContent = "PRODUCT";
  productLi.appendChild(product);

  const sliderLi = document.createElement("li");
  sliderLi.classList.add("header__li");
  const slider = document.createElement("a");
  slider.textContent = "SLIDER";
  sliderLi.appendChild(slider);

  headerlinks.appendChild(woocommerceLi);
  headerlinks.appendChild(productLi);
  headerlinks.appendChild(sliderLi);
  header.appendChild(logoImg);
  header.appendChild(headerlinks);

  const iconList = createElement("ul", header);

  const twitterIcon = createElement("img", iconList, {
    src: twitter,
  });
  const instIcon = createElement("img", iconList, {
    src: inst,
  });
  const facebookIcon = createElement("img", iconList, {
    src: facebook,
  });

  createElement(
    "span",
    header,
    {
      class: "cart",
    },
    "CART(0)"
  );

  return header;
};

export default createHeader;
